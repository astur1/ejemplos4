﻿DROP DATABASE IF EXISTS ejemplo4programacionc;
CREATE DATABASE ejemplo4programacionc;
USE ejemplo4programacionc;

/*
  PROCEDIMIENTO 1
*/
DELIMITER $$
CREATE OR REPLACE PROCEDURE ejemplo1()
  BEGIN
    SELECT "esto es un ejemplo de procedimiento" mensaje;
  END $$
DELIMITER   ;


CALL ejemplo1();

/*
  PROCEDIMIENTO 2
*/
DELIMITER $$
CREATE OR REPLACE PROCEDURE ejemplo2()
  BEGIN
  DECLARE v1 int DEFAULT 1;
  DECLARE v2 varchar(10) DEFAULT NULL;
  DECLARE v3 double(4,2) default 10.48;
  


  SET v1=5;
  SET v2='Ejemplo 2';
  

  SELECT v1, v2, v3, v1*v3;
    
  END $$
DELIMITER ;

CALL ejemplo2();


/*
  PROCEDIMIENTO 3
*/
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo3()
    BEGIN
      /* variables */
      DECLARE v_caracter1 char(1);
      DECLARE forma_pago enum('metálico', 'tarjeta', 'transferencia');
      /* cursores */
      /* control excepciones */
      /* programa */
      SET v_caracter1='hola'; 
      /*
        Como v_caracter1 es 1 caracter por definicion, sólo mostrará el primer caracter 'h'
      */
      SET forma_pago=1;           -- forma_pago devuelve la primera opción del enumerado: metálico
      -- SET forma_pago='cheque'; -- no guarda esta opción porque no está contemplado en la declaración como enumerado
      -- SET forma_pago=4;        -- no está admitida esta opción porque por declaración sólo hay 3 posibles opciones
      SET forma_pago='TARJETA';   -- a forma_pago se le asigna una de las posibles opciones que es tarjeta
      SELECT forma_pago;
      SELECT v_caracter1;
    END $$
  DELIMITER ;
  

CALL ejemplo3();


/*
  PROCEDIMIENTO 4
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo4(IN n1 double)
    BEGIN
  
    SELECT n1;
      
    END $$
  DELIMITER ;
  
CALL ejemplo4(4.3);


/*
  PROCEDIMIENTO 5
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo5(n1 double)
    BEGIN
      DECLARE v1 double;
      SET v1=n1;      
      SELECT V1;

    END $$
  DELIMITER ;
  

CALL ejemplo5(36.2);



/*
  PROCEDIMIENTO 6
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo6(n1 double)
    BEGIN
      DECLARE resultado varchar(15) DEFAULT NULL;
      IF (n1>0) THEN
      	SET resultado=CONCAT(n1, ' es positivo');
      ELSEIF (n1<0) THEN
        SET resultado=CONCAT(n1, ' es negativo');
      ELSE
        SET resultado='Es cero';
      END IF;

      SELECT resultado;
    END $$

  DELIMITER ;
  
CALL ejemplo6(4);


/*
  PROCEDIMIENTO 7
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo7(n1 int)
    BEGIN
      DECLARE resultado varchar(10) DEFAULT 'FALSE';
        IF (MOD(n1,2)=0) THEN
          SET resultado='TRUE';
        ELSE
          SET resultado='FALSE';
        END IF;

        SELECT resultado;
    END $$
  DELIMITER ;
  
CALL ejemplo7(15);


/*
  PROCEDIMIENTO 8
*/

  DELIMITER &&
  CREATE OR REPLACE FUNCTION ejemplo8(n1 double, n2 double)
    RETURNS double
  BEGIN
    DECLARE hipotenusa double DEFAULT 0;
    
    SET hipotenusa=SQRT(POW(n1,2)+POW(n2,2));

       
    RETURN hipotenusa;
  END &&
  DELIMITER ;

  SELECT ejemplo8(5,12);


/*
  PROCEDIMIENTO 9
*/
   DELIMITER $$
   CREATE OR REPLACE PROCEDURE ejemplo9(n1 double, OUT cadena varchar(15))
    BEGIN
      
      IF (n1>0) THEN
      	SET cadena=CONCAT(n1, ' es positivo');
      ELSEIF (n1<0) THEN
        SET cadena=CONCAT(n1, ' es negativo');
      ELSE
        SET cadena='Es cero';
      END IF;

      SELECT cadena;
    END $$

  DELIMITER ;
  
  CALL ejemplo9(-6,@d);


/*
  PROCEDIMIENTO 10
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo10a(n1 double)
    BEGIN
      DECLARE resultado varchar(15) DEFAULT NULL;

      IF (n1>=0 AND n1<5) THEN
        SET resultado='Insuficiente';      	
      ELSEIF (n1>=5 AND n1<6) THEN
        SET resultado='Aprobado';
      ELSEIF (n1>=6 AND n1<7) THEN
        SET resultado='Bien';
      ELSEIF (n1>=7 AND n1<9) THEN
        SET resultado='Notable';
      ELSEIF (n1>=9 AND n1<=10) THEN
        SET resultado='Sobresaliente';      
      ELSE
        SET resultado='nota no válida';      
      END IF;

      SELECT resultado;
    END $$
  DELIMITER ;
  
CALL ejemplo10a(11);


DELIMITER $$
CREATE OR REPLACE PROCEDURE ejemplo10b(n1 double)
  BEGIN
    DECLARE resultado varchar(15) DEFAULT NULL;

    CASE 
      WHEN (n1>=0 AND n1<5) THEN
        SET resultado='Insuficiente'; 
      WHEN (n1>=5 AND n1<6) THEN
        SET resultado='Aprobado';
      WHEN (n1>=6 AND n1<7) THEN
        SET resultado='Bien';
      WHEN (n1>=7 AND n1<9) THEN
        SET resultado='Notable';
      WHEN (n1>=9 AND n1<=10) THEN
        SET resultado='Sobresaliente';
      ELSE 
        SET resultado='nota no válida';
    END CASE;
    
    SELECT resultado;
  END $$
DELIMITER ;

CALL ejemplo10b(5.9);


/*
  PROCEDIMIENTO 11
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo11(nom1 varchar(50), n1 double)
    BEGIN
      CREATE TABLE IF NOT EXISTS notas(
        id int AUTO_INCREMENT,
        nombre varchar(50),
        nota double,
        PRIMARY KEY (id)
        );

      INSERT INTO notas (nombre, nota) VALUES (nom1, n1);
    END $$
  DELIMITER ;
    
  CALL ejemplo11('pepe', 7.9);
  SELECT * FROM notas;


/*
  PROCEDIMIENTO 12
*/

DELIMITER &&
CREATE OR REPLACE FUNCTION ejemplo12(n1 int)
  RETURNS varchar(15)
BEGIN
  DECLARE dia varchar(15) DEFAULT NULL;
  CASE 
      WHEN n1=1 THEN
        SET dia='Lunes';
      WHEN n1=2 THEN
        SET dia='Martes';
      WHEN n1=3 THEN
        SET dia='Miércoles';
      WHEN n1=4 THEN
        SET dia='Jueves';
      WHEN n1=5 THEN
        SET dia='Viernes';
      WHEN n1=6 THEN
        SET dia='Sábado';
      WHEN n1=7 THEN
        SET dia='Domingo'; 
      ELSE 
        SET dia='dia no válido';
    END CASE;
     
  RETURN dia;
END &&
DELIMITER ;

SELECT ejemplo12(6);



/*
  PROCEDIMIENTO 13
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo13(nom1 varchar(50))
    BEGIN
      DECLARE nVeces int DEFAULT 0;

      SET nVeces=(SELECT COUNT(*) FROM notas WHERE nombre=nom1);

      SELECT nVeces;
      
    END $$
  DELIMITER ;
  
  CALL ejemplo13('pepe');


/*
  PROCEDIMIENTO 14
*/
/*
  PROCEDIMIENTO 15
*/
/*
  PROCEDIMIENTO 16
*/
DELIMITER $$
CREATE OR REPLACE PROCEDURE ejemplo16()
  BEGIN
    CREATE TABLE IF NOT EXISTS usuarios(
      id int AUTO_INCREMENT,
      nombre varchar(20),
      contrasenia varchar(20),
      nombreUsuario varchar(20),
      PRIMARY KEY (id),
      UNIQUE KEY (nombreUsuario)
      );
  END $$
DELIMITER ;


CALL ejemplo16();


/*
  PROCEDIMIENTO 17
*/
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo17(nomb varchar(20), passw varchar(20), nombuser varchar(20))
    BEGIN

      INSERT INTO usuarios(
        nombre, contrasenia, nombreUsuario)
        VALUES (nomb,passw,nombuser);

      SELECT * FROM usuarios;      
      
    END $$
  DELIMITER ;
  
CALL ejemplo17('ana','123','anawow');
CALL ejemplo17('pepe','456','pepepo');

/*
  PROCEDIMIENTO 18
*/
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ejemplo18(nom1 varchar(20),passw varchar(20), nomuser varchar(20))
    BEGIN
      CALL ejemplo16();
      CALL ejemplo17(nom1,passw,nomuser);
    END $$
  DELIMITER ;
  
CALL ejemplo18('maria','abc','mari123');


/*
  PROCEDIMIENTO 19
*/
  DELIMITER &&
  CREATE OR REPLACE FUNCTION ejemplo19(nomUser varchar(20))
    RETURNS varchar(10)
  BEGIN
    DECLARE v1 varchar(10) DEFAULT 'FALSO';
    DECLARE nVeces int DEFAULT 0;
    
    SET nVeces=(SELECT COUNT(*) FROM usuarios WHERE nombreUsuario=nomUser);

    IF (nVeces>0) THEN
      SET v1='VERDADERO';
    END IF;

    RETURN v1;
  END &&
  DELIMITER ;

SELECT ejemplo19('anawow');
SELECT ejemplo19('pepe');
