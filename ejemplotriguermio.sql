﻿USE ejemplotrigger;


DELIMITER@@
CREATE OR REPLACE TRIGGER usaDI
  BEFORE INSERT ON usa  
  FOR EACH ROW
  BEGIN
  UPDATE usuario
  SET contador=contador+1
   WHERE idusuario= NEW.idusuario; 
  END@@
  DELIMITER;

TRUNCATE usa;
INSERT INTO usa ( idlocal, idusuario)
  VALUES ( 1,1),(1,2),(1,3);
SELECT * FROM usa;

-- me tiene que colocar en contador las veces que ese usuario sale en la tabla usa 
UPDATE usuario JOIN
(SELECT idusuario, COUNT(*) numero FROM usa GROUP BY idusuario )c1
 ON c1.idusuario= usuario.idusuario
set contador=numero;

-- en usa poner el total segun  el local de usa y los numeros de dias

  
  DELIMITER@@
  CREATE OR REPLACE TRIGGER usaBI
    BEFORE INSERT ON usa 
    FOR EACH ROW
    BEGIN
      SET NEW.total= NEW.dias*(SELECT precio FROM local WHERE NEW.idlocal= idlocal);  
    END@@
    DELIMITER;

TRUNCATE local;
INSERT INTO local ( nombrel, precio)
  VALUES ('l1', 10),('l2',5),('l3',80);
SELECT * FROM local; 
TRUNCATE usa;
INSERT INTO usa (idlocal,idusuario,dias)
  VALUES (1,1,5),(2,1,7),(1,3,80);
SELECT * FROM usa; 


