﻿SET NAMES 'utf8';

DROP DATABASE IF EXISTS ejemplo4programacion;

CREATE DATABASE ejemplo4programacion
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

USE ejemplo4programacion;

DELIMITER $$

--
-- Create procedure `variables`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE variables ()
COMMENT 'se declaran tres variables internas, una integer,otra varchar y otra float'
BEGIN
  DECLARE enter float DEFAULT 4;
  DECLARE tex varchar(10) DEFAULT NULL;
  DECLARE decima float(4) DEFAULT 10.48;
  SET enter = decima * 2;
  SELECT
    decima,
    enter,
    tex;
END
$$

--
-- Create procedure `texto`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE texto ()
BEGIN
  SELECT
    "esto es un mensaje de texto" mensaje;
END
$$
DELIMITER ;
CALL texto();

DELIMITER //
CREATE OR REPLACE PROCEDURE numero(n1 double)
  COMMENT 'recibe un parametro y devuelve un numero'
  BEGIN
   SELECT n1;
  END //
DELIMITER ;
CALL numero(2.45);

DELIMITER //
CREATE OR REPLACE PROCEDURE var(n1 int)
  COMMENT 'recibe un parametro y asigna el parametro a una variable del mismo tipo y despues la muestra'
  BEGIN
   DECLARE variable int;
  SET variable= n1;
  SELECT variable;
  END //
DELIMITER ;
CALL var(3);

DELIMITER //
CREATE OR REPLACE PROCEDURE tipo(n1 double)
  COMMENT 'recibe un numero real de entrada y muestra un mensaje indicando si es positivo,negativo o cero'
  BEGIN
 IF n1=0 THEN SELECT "es cero";
ELSEIF n1>0 THEN SELECT "es mayor que cero";
ELSE SELECT"es menor que cero";
END IF;
  END //
DELIMITER ;
CALL tipo(0);

 DELIMITER //
 CREATE OR REPLACE FUNCTION bol(n1 int)
   RETURNS int
  COMMENT 'recibe un numero de entrada y devuelve true o false si es par o impar' 
   BEGIN
  DECLARE r bool;
  IF MOD(n1,2) = 0 THEN 
    SET r = TRUE;
    ELSE
    SET r=FALSE;
  END if;
      RETURN r;
   END//
 DELIMITER;

SELECT bol(5);

 DELIMITER //
 CREATE OR REPLACE FUNCTION hipotenusa(n1 int,n2 int)
   RETURNS float
  COMMENT 'recibe dos argumentos y devuelve la hipotenusa de un triangulo'
   BEGIN
      DECLARE hipo float  DEFAULT 0;
      SET hipo= SQRT(POW(n1,2)+POW(n2,2))  ;
      RETURN hipo ;
   END//
 DELIMITER;

SELECT hipotenusa(5,12) ;

DELIMITER //
CREATE OR REPLACE PROCEDURE tipo2(IN n1 double, OUT indicoque varchar(15) )
  COMMENT 'igual que el ejercicio 6 pero con un parametro de entrada y otro de salida indicando si es positivo,negativo o cero'
  BEGIN
   IF (n1<0) THEN SET indicoque= CONCAT( n1," es negativo");
  ELSEIF(n1>0) THEN SET indicoque= CONCAT(n1," es positivo");
  ELSE SET indicoque= CONCAT(n1," es cero");
  END IF;
  SELECT indicoque;
  END //
DELIMITER ; 
CALL tipo2(5,@g);

 DELIMITER //
 CREATE OR REPLACE PROCEDURE nota(n1 double)
  COMMENT 'recibe un numero real y devuelve la nota del alumno si sobrepasa determinada puntuacion'
   BEGIN
    IF n1 <5 THEN SELECT "insuficiente";
    ELSEIF n1 <6 THEN SELECT "aprobado";
    ELSEIF n1 <7 THEN SELECT "bien";
    ELSEIF n1 <9 THEN SELECT "notable";
    ELSEIF n1 <=10 THEN SELECT "sobresaliente";
    ELSE SELECT "nota no valida";
    END IF;
   END //
 DELIMITER ;
CALL nota(11);

DELIMITER //
CREATE OR REPLACE PROCEDURE nnota(nom1 varchar(50),not2 double)
  COMMENT 'recibe dos argumentos, uno entero y otro varchar y los devuelve en una tabla creada'
  BEGIN
   CREATE TABLE IF NOT EXISTS notas(
  id int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(50),
    nota double
  );
  INSERT INTO notas (id, nombre, nota) VALUES
    ( DEFAULT, nom1, not2);
  SELECT * FROM notas n;
  END //
DELIMITER ;
CALL nnota("nota",6);

 DELIMITER //
 CREATE OR REPLACE FUNCTION semana(n1 int)
   RETURNS varchar(15)
   BEGIN

      IF n1=1 THEN SELECT "lunes";
      ELSEIF n1=2 THEN SELECT "martes";
      ELSEIF n1=3 THEN SELECT "miercoles";
      ELSEIF n1=4 THEN SELECT "jueves";
      ELSEIF n1=5 THEN SELECT "viernes";
      ELSEIF n1=6 THEN SELECT "sabado";
      ELSEIF n1=7 THEN SELECT "domingo";
      ELSE SELECT "no valido";
      END IF;
      RETURN n1 ;
   END//
 DELIMITER;

SELECT dia(5) ;
 

 DELIMITER //
 CREATE OR REPLACE FUNCTION dia(n1 int )
   RETURNS varchar(15)
  COMMENT 'funcion que le das un parametro y te devuelve un mensaje con el dia de la semana segun el numero que le pongas'
   BEGIN
    DECLARE dia varchar(15) DEFAULT NULL;
     CASE
      WHEN n1=1 THEN
      SET dia='lunes';
      WHEN n1=2 THEN
      SET dia='martes';
      WHEN n1=3 THEN
      SET dia='miercoles';
      WHEN n1=4 THEN
      SET dia='jueves';
      WHEN n1=5 THEN
      SET dia='viernes';
      WHEN n1=6 THEN
      SET dia='sabado';
      WHEN n1=7 THEN
      SET dia='domingo';
      ELSE SET dia='no valido';
      END CASE;
      RETURN dia ;
   END//
 DELIMITER;

SELECT dia(8) ; 

DELIMITER //
CREATE OR REPLACE PROCEDURE alu (n1 varchar(20))
  COMMENT 'Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva 
  cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo sin utilizar cursores.'
  BEGIN
   SELECT COUNT(*) FROM notas WHERE n1= nombre;
  END //
DELIMITER ;    
CALL alu('nom1'); 

                            

