﻿USE ejemplotrigger;


CREATE TABLE local(
idlocal int AUTO_INCREMENT PRIMARY KEY,
  nombrel varchar(20),
  precio int,
  plazas int
);
CREATE TABLE usuario(
idusuario int AUTO_INCREMENT PRIMARY KEY,
  nombreu varchar(30),
  inicial char(1)
);
CREATE TABLE usa(
fecha date,
  dias int,
  total float,
  idlocal int,
  idusuario int,
  PRIMARY KEY(idlocal, idusuario)
  );


-- cuando introduzco un usuario me calcule y almacene la inicial de su nombre
DELIMITER@@
CREATE OR REPLACE TRIGGER usuariobi
  BEFORE INSERT ON usuario  
  FOR EACH ROW
  BEGIN
 SET NEW.inicial=LEFT(new.nombreu,1);
  END@@
  DELIMITER;
INSERT INTO usuario ( nombreu, inicial)
  VALUES ( 'ramon', '');
SELECT * FROM usuario; 