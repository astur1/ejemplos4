﻿USE ejemplotrigger;


DELIMITER@@
CREATE OR REPLACE TRIGGER usaDI
  BEFORE INSERT ON usa  
  FOR EACH ROW
  BEGIN
  UPDATE usuario
  SET contador=contador+1
   WHERE idusuario= NEW.idusuario; 
  END@@
  DELIMITER;

TRUNCATE usa;
INSERT INTO usa ( idlocal, idusuario)
  VALUES ( 1,1),(1,2),(1,3);
SELECT * FROM usa;

-- me tiene que colocar en contador las veces que ese usuario sale en la tabla usa 
UPDATE usuario JOIN
(SELECT idusuario, COUNT(*) numero FROM usa GROUP BY idusuario )c1
 ON c1.idusuario= usuario.idusuario
set contador=numero;


