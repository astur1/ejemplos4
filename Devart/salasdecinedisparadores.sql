﻿USE programacion;

CREATE OR REPLACE TABLE salas(
id int AUTO_INCREMENT PRIMARY KEY,
  butacas int,
  fecha date,
  edad int DEFAULT 0
);

CREATE OR REPLACE TABLE ventas(
id int AUTO_INCREMENT PRIMARY KEY,
  sala int,
  numero int DEFAULT 0
);



-- creamos un disparador
DELIMITER@@
CREATE OR REPLACE TRIGGER disparador1
  AFTER INSERT ON salas FOR EACH ROW;

  -- disparador de sala que me calcule la edad de la sala 
  -- en funcion de su fecha de alta CON INSERT para introducir datos

DELIMITER@@
CREATE OR REPLACE TRIGGER salasBi
  BEFORE INSERT ON salas 
  FOR EACH ROW
  BEGIN
  SET NEW.edad=TIMESTAMPDIFF(year,NEW.FECHA,NOW());
 END@@
DELIMITER;

INSERT INTO salas(butacas, fecha)
  VALUES (50,"2010/1/1");
SELECT * FROM salas;

-- lo mismo que el anterior pero con update para actualizar 

DELIMITER@@
CREATE OR REPLACE TRIGGER salasBU
  BEFORE UPDATE ON salas 
  FOR EACH ROW
  BEGIN
  SET NEW.edad=TIMESTAMPDIFF(year,NEW.FECHA,NOW());
  
 END@@
DELIMITER;


  UPDATE salas
    SET fecha="2002/1/1";
    
SELECT * FROM salas;

-- ejemplo: necesito que la tabla salas disponga de tres campos nuevos
-- esos campos seran dia,mes,año
-- quiero que esos tres campos automaticamente tengan el dia,mes y año de la fecha
-- necesito un disparador para insertar y otro para actualizar

ALTER TABLE salas
  ADD COLUMN dia int,
  ADD COLUMN mes int,
  ADD COLUMN anyo int;

DELIMITER@@
CREATE OR REPLACE TRIGGER salasBi
  BEFORE INSERT ON salas 
  FOR EACH ROW
  BEGIN
  SET NEW.edad=TIMESTAMPDIFF(year,NEW.FECHA,NOW());
  SET new.dia=DAY(new.fecha);
  SET NEW.mes=MONTH(new.fecha);
  SET NEW.anyo=YEAR(new.fecha);   
 END@@
DELIMITER;

INSERT INTO salas(butacas, fecha)
  VALUES (50,"2010/1/1");
SELECT * FROM salas;


CREATE OR REPLACE TABLE ventas1(
  id int AUTO_INCREMENT PRIMARY KEY,
  fecha date,
  precioUnitario float,
  unidad int,
  precioFinal float
  );

DELIMITER@@
CREATE OR REPLACE TRIGGER ventas1bi 
  BEFORE INSERT ON ventas1 
  FOR EACH ROW
  BEGIN
  SET NEW.precioFinal=NEW.precioUnitario*new.unidad;
 END@@
DELIMITER;



DELIMITER@@
CREATE OR REPLACE TRIGGER ventas1bu 
  BEFORE UPDATE ON ventas1 
  FOR EACH ROW
  BEGIN
  SET NEW.precioFinal=NEW.precioUnitario*new.unidad;
 END@@
DELIMITER; 

INSERT INTO ventas1 ( fecha, precioUnitario, unidad) 
  VALUES ("2012/01/01", 5, 8);
SELECT * FROM ventas1; 
UPDATE ventas1 SET precioUnitario=10 WHERE id=2;
SELECT * FROM ventas1;


-- crear un disparador para la tabla ventas1 para que cuando metas un registro nuevo te calcule el total automaticamente
DELIMITER//
CREATE OR REPLACE TRIGGER ventas1c
  BEFORE INSERT ON ventas1  
  FOR EACH ROW
  BEGIN
  SET NEW.precioFinal=NEW.precioUnitario*new.unidad;
  END//
  DELIMITER;



