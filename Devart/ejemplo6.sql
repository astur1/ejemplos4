﻿     
     USE ejemplo6;

-- 1. Crear un disparador para la tabla ventas para que cuando metas un registro nuevo te
-- calcule el total automáticamente.
  
  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBI
    BEFORE INSERT
    ON ventas
    FOR EACH ROW
    BEGIN
      SET NEW.total = NEW.unidades * NEW.precio;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  INSERT INTO ventas (producto, precio, unidades)
    VALUES ('p6', 15, 20);

  SELECT * FROM ventas v;

-- 2. Crear un disparador para la tabla ventas para que cuando inserte un registro me sume
-- el total a la tabla productos (en el campo cantidad).

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAI
    AFTER INSERT
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos p 
        SET p.cantidad = p.cantidad + NEW.total 
        WHERE p.producto = NEW.producto;
    END //
  DELIMITER        

                                                ;

  -- Comprobamos su funcionamiento
  INSERT INTO ventas (producto, precio, unidades)
    VALUES ('p3', 50, 10);

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

-- 3. Crear un disparador para la tabla ventas para que cuando actualices un registro nuevo
-- te calcule el total automáticamente.

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBU
    BEFORE UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      SET NEW.total = NEW.unidades * NEW.precio;
    END //
  DELIMITER;

  -- Comprobamos su funcionamiento
  UPDATE ventas v 
    SET v.unidades = 100
    WHERE v.id = 8;

  SELECT * FROM ventas v;

-- 4. Crear un disparador para la tabla ventas para que cuando actualice un registro me
-- sume el total a la tabla productos (en el campo cantidad).

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAU
    AFTER UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos p 
        SET p.cantidad = p.cantidad + (NEW.total - OLD.total) 
        WHERE p.producto = NEW.producto;
    END //
  DELIMITER;

  -- Comprobamos su funcionamiento
  UPDATE ventas v 
    SET v.unidades = 50
    WHERE v.id = 8;

  SELECT * FROM ventas v;
  SELECT * FROM productos p;


-- 5. Crear un disparador para la tabla productos que si cambia el código del producto te
-- sume todos los totales de ese producto de la tabla ventas

  DELIMITER //
  CREATE OR REPLACE TRIGGER productosBU
    BEFORE UPDATE ON
    productos
    FOR EACH ROW
    BEGIN
      SET NEW.cantidad = (
        SELECT SUM(v.total) FROM ventas v
          WHERE v.producto = NEW.producto);
    END //
  DELIMITER;
  
  -- Comprobamos su funcionamiento
  UPDATE productos p 
    SET p.producto = 'p5'               
    WHERE p.producto = 'p8';

  SELECT * FROM productos p;
  SELECT * FROM ventas; 

-- 6. Crear un disparador para la tabla productos que si eliminas un producto te elimine
-- todos los productos del mismo código en la tabla ventas

  DELIMITER //
  CREATE OR REPLACE TRIGGER productosAD
    AFTER DELETE
    ON productos
    FOR EACH ROW
    BEGIN
      DELETE FROM ventas
        WHERE OLD.producto = producto;
    END //
  DELIMITER;

  -- Comprobamos su funcionamiento
  DELETE FROM productos 
    WHERE producto = 'p2';

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

-- 7. Crear un disparador para la tabla ventas que si eliminas un registro te reste el total del
-- campo cantidad de la tabla productos (en el campo cantidad).

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAD
    AFTER DELETE
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos  
        SET cantidad = cantidad - OLD.total 
        WHERE producto = OLD.producto;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  DELETE FROM ventas 
    WHERE id = 5;

  DELETE FROM ventas 
    WHERE id = 14;

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

-- 8. Modificar el disparador 3 para que modifique la tabla productos actualizando el valor
-- del campo cantidad en funcion del total.

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBU
    BEFORE UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      SET NEW.total = NEW.unidades * NEW.precio;
      UPDATE productos p 
        SET p.cantidad = p.cantidad + (NEW.total - OLD.total) 
        WHERE p.producto = NEW.producto;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  UPDATE ventas v 
    SET v.unidades = 120
    WHERE v.id = 8;

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

-- 1 crear un disparador para que cuando inserte un registro en clientes me compruebe si la edad
  -- está entre 18 y 70. en caso de que no este produzca una excepción con el mensaje "la edad no es valida"
  
  DELIMITER@@
  CREATE OR REPLACE TRIGGER edadBI
    BEFORE INSERT ON  
    FOR EACH ROW
    BEGIN
    
    END@@
    DELIMITER;
