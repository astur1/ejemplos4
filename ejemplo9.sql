﻿-- 1a- se crea un script que te genera la base de datos --
DROP DATABASE IF EXISTS ejemplo9;
CREATE DATABASE IF NOT EXISTS ejemplo9;
USE ejemplo9;

/*

CREATE OR REPLACE TABLE poblacion(
codigo int PRIMARY KEY,
  nombre varchar(40),
  numempresa int,
  numtrabajador int
) ENGINE = MYISAM,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

CREATE OR REPLACE TABLE trabajan(
fechaini date,
  fechafin date,
idemple int,
  idempresa int,
  baja boolean,
);

  */
DELIMITER //
CREATE OR REPLACE PROCEDURE emple(test boolean)
  COMMENT '1b- procedimiento almacenado para crear la tabla empleados
  (me permite introducir datos en la tabla a traves de un argumento booleano llamado test)'
  BEGIN
  IF test= TRUE THEN 
    CREATE OR REPLACE TABLE empleados(
      idemple int AUTO_INCREMENT PRIMARY KEY,
      nombremple varchar(30),
      edad int,
      correo varchar(30),
      fnac date)
      -- para que cree el los constrains se llama a engine=myisam así 
    ENGINE = MYISAM
    CHARACTER SET utf8
    COLLATE utf8_spanish_ci;
  END IF;  
  END //
DELIMITER ;
-- se llama a la tabla empleados con el argumento booleano
CALL emple(1);

DELIMITER //
CREATE OR REPLACE PROCEDURE empresa (test boolean)
  COMMENT '1c- procedimiento almacenado para crear la tabla empresa
(me permite introducir datos en la tabla a traves de un argumento booleano lla mado test)'
  BEGIN
  CREATE OR REPLACE TABLE empresa(
     idempresa int AUTO_INCREMENT PRIMARY KEY,
     nombrempresa varchar(40),
     direccion varchar(40),
     numemple int)
    ENGINE = MYISAM
    CHARACTER SET utf8
    COLLATE utf8_spanish_ci;
    IF test=TRUE THEN
    INSERT empresa ( nombrempresa, direccion, numemple)
  VALUES ('muebles', 'c/abajo', 5);
    END IF ;
  END //
DELIMITER ;
-- se llama a la tabla empresa con el argumento booleano
CALL empresa(1);
SELECT * FROM empresa;

-- 2 insertar 5 empleados (solo en los campos no derivados)
SELECT * FROM empleados;
TRUNCATE empleados;
INSERT INTO empleados ( nombremple, fnac)
  VALUES ( 'juan', '1989/12/02'),('jose','1998/07/03')
,('pepe','2020/08/06'),('luis','2011/04/09'),('teo','1975/12/06');

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizar ()
  COMMENT 'procedimiento que me actualice la tabla empleados
  calculando los campos edad y correo
  (el campo edad en funcion de la fecha de nacimiento
  y el campo correo utilizando "nombre@prueba.es")'
 BEGIN
   UPDATE empleados SET edad= TIMESTAMPDIFF(year, fnac, CURDATE());
   UPDATE empleados SET correo="nombre@prueba.es";
  END //
DELIMITER ;
-- para llamar al procedimiento se pone
  CALL actualizar();
  SELECT * FROM empleados; 

  
  DELIMITER@@
-- utilizando disparadores conseguir que en la tabla empleados cuando 
-- introduzcamos o modifiquemos un registro nos calcule la edad y el correo
-- a
  CREATE OR REPLACE TRIGGER empleBI 
    BEFORE INSERT ON empleados  
    FOR EACH ROW
    BEGIN
    SET NEW.edad=TIMESTAMPDIFF(year,fnac,CURDATE());
    SET NEW.correo='nombre@prueba.es';
    END@@
    DELIMITER;
  TRUNCATE empleados;
  SELECT * FROM empleados; 

  -- b 
    
    DELIMITER@@
     CREATE OR REPLACE TRIGGER empleBU 
      BEFORE UPDATE ON empleados  
      FOR EACH ROW
      BEGIN
      SET NEW.edad=TIMESTAMPDIFF(year, fnac,CURDATE());
      SET NEW.correo='nombre@prueba.es';
      END@@
      DELIMITER;
  TRUNCATE empleados;
  SELECT * FROM empleados; 

  -- 5 introducir cinco empresas (solo en los campos no derivados)
    INSERT INTO empresa ( nombrempresa, direccion)
  VALUES ( 'sastre', 'desastre'),('puli','pulidor'),('pin','pintor')
,('alba','albañil'),('jar','jardin');
SELECT * FROM empresa; 

-- 6 introducir cinco registros en trabajan (solo en los campos no derivados)
 CREATE OR REPLACE TABLE trabajan(
fechaini date,
  fechafin date,
idemple int,
  idempresa int,
  baja boolean,
PRIMARY KEY( idemple, idempresa));
INSERT INTO trabajan (fechaini, fechafin, idemple, idempresa)
  VALUES ('1998/03/05','2003/04/05', 2, 1), ('1988/11/05','2005/64/05', 1, 2),
('1948/03/05','2004/04/05', 1, 1),('1998/03/05',NULL, 6, 1),
('1968/03/05','2003/04/05', 3, 1);
SELECT * FROM trabajan; 

-- 7 realizar procedimientos o triggers que nos permitan actualizar los campos que quedan
  
  DELIMITER@@
  CREATE OR REPLACE TRIGGER trabajanBI 
    BEFORE INSERT ON trabajan  
    FOR EACH ROW
    BEGIN
    IF new.fechafin IS NULL THEN
      SET NEW.baja=FALSE;
      ELSE SET NEW.baja=TRUE; 
      END IF;
    END@@
    DELIMITER;
SELECT * FROM trabajan; 
TRUNCATE trabajan;


